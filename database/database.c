#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/stat.h>
#define PORT 8080
#define ROOT 0

char username[25];
char password[25];

struct user{
    char name[25];
    char password[25];
    char permittedDB[512];
};

struct db{
    char name[25];
};

void createUser(char name[], char password[])
{
    struct user newUser;
    char buffer[100];
    FILE *fptr;

    strncpy(newUser.name, name, 25);
    strncpy(newUser.password, password, 25);

    if((fptr = fopen("./database/databases/userDB/user.txt", "a+")) == NULL)
    {
        perror("Error opening file");
        exit(1);
    }
    fwrite(&newUser, sizeof(struct user), 1, fptr);
    if(fwrite != 0)
        printf("Contents of the file successfuly written");
    else
    {
        perror("Error writing file!\n");
        exit(1);
    }

    fclose(fptr);
    return;
}

void addPermision(struct user *userPtr, char dbName[])
{
    strcat(userPtr->permittedDB, dbName);
    strcat(userPtr->permittedDB, ";");
    return;
}

struct user *getUser()
{
    struct user userCheck;
    FILE *fptr = fopen("./database/databases/userDB/user.txt", "r");
    if(fptr == NULL)
    {
        perror("Error opening file in getUser\n");
        exit(1);
    }
    while(fread(&userCheck, sizeof(struct user), 1, fptr))
    {
        if((strcmp(username, userCheck.name) == 0) || (strcmp(password, userCheck.password) == 0))
        {
            struct user *userReturn = (struct user*) malloc(sizeof(struct user));
            memcpy(userReturn, &userCheck, sizeof(struct user));
            return userReturn;
        }
    }

    return NULL;
}

bool isPermitted(char currDatabase[])
{
    uid_t uid = getuid();
    struct user *currUser;
    if(uid == ROOT)
    {
        return true;
    }
    if((currUser = getUser()) != NULL)
    {
        char *token;
        token = strtok(currUser->name, ';');

        while(token != NULL)
        {
            if(strcmp(currDatabase, token) == 0)
                return true;
        }
    }
    else
    {
        perror("Username or password incorrect!");
        exit(1);
    }
    return false;
}

void createDatabase(char dbName[])
{
    if(isPermitted(dbName))
    {
        char dirTemp[50];
        snprintf(dirTemp, 50, "./database/databases/%s", dbName);
        mkdir(dirTemp, 777);
    }
    else
    {
        printf("Error, user not permitted.\n");
    }
    return;
}

void dropDatabase(char dbName[])
{
    if(isPermitted(dbName))
    {
        char dirTemp[50];
        snprintf(dirTemp, 50, "./database/databases/%s", dbName);
        rmdir(dirTemp);
    }
    else
    {
        printf("Error, user not permitted.\n");
    }
    return;
}

int main(int argc, char const *argv[]) {
    int server_fd, new_socket, valread;
    struct sockaddr_in address;
    int opt = 1;
    int addrlen = sizeof(address);
    char buffer[1024] = {0};
    char *hello = "Hello from server";
      
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }
      
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );
      
    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }

    valread = read( new_socket , buffer, 1024);
    printf("%s\n",buffer );
    send(new_socket , hello , strlen(hello) , 0 );
    printf("Hello message sent\n");

    memset(buffer, '0', 1024);
    // Will check for username and password
    valread = read(new_socket, buffer, 1024);
    
    return 0;
}